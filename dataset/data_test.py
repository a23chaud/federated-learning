from .data import distribute_clients


def test_distribute_clients() -> None:
    """Test distribute_clients."""

    # Assert
    assert sum(distribute_clients(10, [100, 200, 300])) == 10
    assert distribute_clients(10, [200, 200, 300, 300]) == [2, 2, 3, 3]
    assert distribute_clients(10, [100, 200, 400, 300], min=2) == [2, 2, 3, 3]
    assert distribute_clients(10, [3569, 880623, 960078, 359618], min=2) == [2, 3, 3, 2]
