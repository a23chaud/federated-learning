"""Trust-FIDS dataset module."""

from .data import BatchLoader, Dataset
