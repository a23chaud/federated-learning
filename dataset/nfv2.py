import math
from typing import List, Optional, Tuple

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

from .data import Dataset


def load_data(
    dataset_path: str,
    test_ratio: float = 0.2,
    n_partition: int = 1,
    seed: Optional[int] = None,
    is_origin: bool = False,
    only_benign: bool = True,
) -> Tuple[Dataset, Dataset] or List[Tuple[Dataset, Dataset]]:
    """Load data from a dataset path.

    Args:
        dataset_path (str): Path to the dataset.
        test_size (float): Part of the dataset to use for testing.
        n_partition (int): Number of partitions to split the dataset into.
        seed (int): Seed for reproducibility, defaults to None for randomness.

    Returns:
        One of the following:
            * Tuple of Datasets if `n_partitions == 1`.
            * List of Tuples of Datasets if `n_partitions > 1`.
    """
    df = pd.read_csv(dataset_path, low_memory=True)

    # columns to drop
    cols = [
        "IPV4_SRC_ADDR",
        "L4_SRC_PORT",
        "IPV4_DST_ADDR",
        "L4_DST_PORT",
        "Label",
        "Attack",
    ]
    if not is_origin:
        cols += ["Dataset"]

    d = clean_and_scale(
        df, cols
    )

    data = split_and_partition(
        d,
        test_ratio=test_ratio,
        n_partition=n_partition,
        seed=seed,
        only_benign=only_benign,
    )

    if n_partition == 1:
        return data[0]

    return data


def clean_and_scale(
    df: pd.DataFrame,
    rm_cols: List[str],
    test_df: Optional[pd.DataFrame] = None,
) -> Dataset:
    """Clean and scale a dataset for Deep Learning applications.

    Args:
        df (pd.DataFrame): Dataframe to prepare.
        test_df (pd.DataFrame): Testing Dataframe, if any.
            Unused in this function.
        columns (list): List of columns to drop.

    Returns:
        pd.DataFrame: Cleaned, normalized Dataframe without the dropped columns.
    """

    X = df.drop(columns=rm_cols)
    y = df["Label"]
    m = df[rm_cols]

    # convert classes to numerical values
    X = pd.get_dummies(X)

    scaler = MinMaxScaler()
    scaler.fit(X)
    X[X.columns] = scaler.transform(X)

    return Dataset(X, y, m)


def split_and_partition(
    d: Dataset,
    test_ratio: float,
    n_partition: int,
    seed: int or None,
    only_benign: bool = True,
) -> List[Tuple[Dataset, Dataset]]:
    """Split the dataset into training and testing sets for n partitions.

    Args:
        X (pd.DataFrame): Dataset samples (X).
        y (pd.DataFrame): Dataset labels (y).
        test_ratio (float): Part of the dataset to use for testing.
        n_partition (int): Number of partitions to split the dataset into.
        seed (int): Seed for reproducibility.

    """
    assert 0.0 <= test_ratio <= 1.0, "`test_ratio` must be between 0.0 and 1.0."
    assert n_partition > 0, "`n_partition` must be greater than 0."

    X_train: pd.DataFrame
    X_test: pd.DataFrame
    y_train: pd.DataFrame
    y_test: pd.DataFrame
    m_train: pd.DataFrame
    m_test: pd.DataFrame

    # Split data into training and testing sets.
    X_train, X_test, y_train, y_test, m_train, m_test = train_test_split(
        *d, test_size=test_ratio, random_state=seed, stratify=d.m["Attack"]
    )  # type: ignore

    # Remove malicious samples from training set.
    # This probably has to be done here, because otherwise, they are chances
    # that clients might get only malicious samples in their training set.
    if only_benign:
        benign = y_train == 0
        X_train = X_train[benign]
        y_train = y_train[benign]
        m_train = m_train[benign]

    train_sets = partition(Dataset(X_train, y_train, m_train), n_partition)
    test_sets = partition(Dataset(X_test, y_test, m_test), n_partition)

    return list(zip(train_sets, test_sets))


# def partition(d: Dataset, n_partition: int) -> List[Dataset]:
#     """Partition the dataset into n balanced partitions.

#     Args:
#         d (Dataset): Dataset to partition.
#         n_partition (int): Number of partitions to split the dataset into.
#     """

#     sss = StratifiedShuffleSplit(n_splits=5, test_size=0.5, random_state=0)
#     for train_index, test_index in sss.split(X, y):
#         print("TRAIN:", train_index, "TEST:", test_index)
#         X_train, X_test = X[train_index], X[test_index]
#         y_train, y_test = y[train_index], y[test_index]


def partition(d: Dataset, n_partition: int) -> List[Dataset]:
    """Partition the dataset into n partitions.

    Args:
        X (pd.DataFrame): Dataset samples (X).
        y (pd.DataFrame): Dataset labels (y).
        n_partition (int): Number of partitions to split the dataset into.
    """

    partition_size = math.floor(len(d.X) / n_partition)
    partitions = []
    for i in range(n_partition):
        idx_from, idx_to = i * partition_size, (i + 1) * partition_size

        X_part = d.X[idx_from:idx_to]
        y_part = d.y[idx_from:idx_to]
        m_part = d.m[idx_from:idx_to]

        partitions.append(Dataset(X_part, y_part, m_part))

    return partitions


def load_siloed_data(
    D: List[str],
    clients: List[int],
    seed: int,
    only_benign: bool = True,
) -> List[Tuple[Dataset, Dataset]]:
    """Create datasets for N clients.

    Args:
        D (List[str]): List of dataset paths.
        clients (List[Scalar]): List of number of clients per dataset,
            with len(D) == len(clients).
        seed (int): Seed for reproducibility.

    Returns:
        List[Tuple[Dataset,Dataset]]: List of datasets for each client.
    """
    assert len(D) == len(
        clients
    ), "Number of datasets and number of clients must be equal."

    data = []
    for d, n in zip(D, clients):
        l = load_data(
            d,
            seed=seed,
            n_partition=n,
            is_origin=("origin" in d),
            # attack data is required to train the EFC
            only_benign=only_benign,
        )
        data.extend(l if n > 1 else [l])

  
    return data


# def add_efc(
#     datasets: Tuple[Dataset, Dataset], benign_efc: bool, norm_efc: bool
# ) -> Tuple[Dataset, Dataset]:
#     """Add EFC to the dataset.

#     Args:
#         datasets (Tuple[Dataset, Dataset]): Tuple of train and test datasets.
#         benign_efc (bool): Whether to train EFC on benign samples only.
#         norm_efc (bool): Whether to normalize the energies.

#     Returns:
#         Tuple[Dataset, Dataset]: Tuple of train and test datasets with EFC.
#     """
#     train, test = datasets
#     if not any(train.y == 1):
#         print("No attack samples in training set, training EFC only on benign samples.")
        

#     X_efc = train.X.copy()
#     y_efc = train.y.copy()
#     if benign_efc:
#         # EFC is typically trained using labels. However, in this case we aim at
#         # unsupervised learning only. This is to test the effect of using only
#         # benign samples for training EFC.
#         filt = y_efc == 0
#         X_efc = X_efc[filt]
#         y_efc = y_efc[filt]  # type: ignore

#     ebfc = EnergyBasedFlowClassifier(cutoff_quantile=0.95)
#     ebfc.fit(X_efc, y_efc)

#     _, y_train_energies = ebfc.predict(train.X, return_energies=True)
#     _, y_test_energies = ebfc.predict(test.X, return_energies=True)
#     y_train_energies = y_train_energies.reshape(-1, 1)
#     y_test_energies = y_test_energies.reshape(-1, 1)

#     if norm_efc:
#         # Normalize energies to [0, 1] using min-max scaling
#         scaler = MinMaxScaler()
#         scaler.fit(y_train_energies)
#         y_train_energies = scaler.transform(y_train_energies)
#         y_test_energies = scaler.transform(y_test_energies)

#     train.X["ENERGY"] = y_train_energies
#     test.X["ENERGY"] = y_test_energies

#     filt = train.y == 0
#     train.X = train.X[filt]
#     train.y = train.y[filt]  # type: ignore
#     train.m = train.m[filt]

#     return train, test
