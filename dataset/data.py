"""NF-V2 Dataset utilities.

This module contains functions to load and prepare the NF-V2 dataset for
Deep Learning applications. The NF-V2 dataset is a collection of 4 datasets
with a standardised set of features. The datasets are:
    * CSE-CIC-IDS-2018
    * UNSW-NB15
    * ToN-IoT
    * Bot-IoT

The NF-V2 dataset is available at:
    https://staff.itee.uq.edu.au/marius/NIDS_datasets/

Part of the code in this module is based on the code from Bertoli et al. (2022),
who tested Federated Learning on the NF-V2 dataset.

The code is available at:
    https://github.com/c2dc/fl-unsup-nids

References:
    * Sarhan, M., Layeghy, S. & Portmann, M., Towards a Standard Feature Set for
      Network Intrusion Detection System Datasets. Mobile Netw Appl (2021).
      https://doi.org/10.1007/s11036-021-01843-0 
    * Bertoli, G., Junior, L., Santos, A., & Saotome, O., Generalizing intrusion
      detection for heterogeneous networks: A stacked-unsupervised federated
      learning approach. arXiv preprint arxiv:2209.00721 (2022).
      https://arxiv.org/abs/2209.00721
"""


import math
from collections.abc import Iterable
from dataclasses import dataclass
from typing import List, Optional, Tuple

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.utils import shuffle as sk_shuffle

keras = tf.keras
from keras.utils import Sequence


class BatchLoader(Sequence):
    """Generator of batches for training."""

    X: pd.DataFrame
    target: pd.DataFrame or pd.Series or None

    batch_size: int

    def __init__(
        self,
        batch_size: int,
        X: pd.DataFrame,
        target: pd.DataFrame or pd.Series or None = None,
        shuffle: bool = False,
        seed: int or None = None,
    ):
        """Initialise the BatchLoader."""
        self.batch_size = batch_size

        self.X = X
        self.target = target if target is not None else X.copy()

        if shuffle:
            indices = np.arange(len(X))
            if seed is not None:
                np.random.seed(seed)
            np.random.shuffle(indices)

            self.X = X.iloc[indices]
            self.target = self.target.iloc[indices]

    def __len__(self):
        return int(np.ceil(len(self.X) / float(self.batch_size)))

    def __getitem__(self, idx):
        low = idx * self.batch_size
        # Cap upper bound at array length; the last batch may be smaller
        # if the total number of items is not a multiple of batch_size.
        high = min(low + self.batch_size, len(self.X))

        batch_x = self.X[low:high]
        if self.target is None:
            return batch_x

        batch_target = self.target[low:high]

        # A Sequence should apparently return a tuple of NumPy arrays, as DataFrames
        # cause errors in the fit() method.
        return batch_x.to_numpy(), batch_target.to_numpy()


@dataclass
class Dataset:
    """Dataset class."""

    X: pd.DataFrame
    y: pd.DataFrame or pd.Series
    m: pd.DataFrame

    def __getitem__(self, key):
        """Overload the [] operator to access the dataset attributes.

        This allows to consider a Dataset object as a tuple of the form (X, y, m),
        enabling easier access to the attributes:
            * X = dataset[0]
            * X, y, m = dataset
            * some_function(*dataset)
            * ...
        """
        if key == 0:
            return self.X
        elif key == 1:
            return self.y
        elif key == 2:
            return self.m
        else:
            raise IndexError()

    def __len__(self):
        return len(self.X)

    def to_sequence(
        self,
        batch_size: int,
        target: int or None = None,
        seed: int or None = None,
        shuffle: bool = False,
    ) -> BatchLoader:
        """Convert the dataset to a BatchLoader object.

        Args:
            batch_size (int): Size of the batches.
            target (int): Target to use for the batches, defaults to None. 0 for X, 1
                for y, 2 for m.

        Returns:
            BatchLoader: BatchLoader object.
        """
        if target is None:
            return BatchLoader(batch_size, self.X, seed=seed, shuffle=shuffle)
        if 0 <= target <= 2:
            return BatchLoader(
                batch_size, self.X, self[target], seed=seed, shuffle=shuffle
            )
        raise IndexError("If not None, parameter `target` must be in [0, 2]")


# def round_keep_sum(n: List) -> List[int]:
#     """Round a list of floats to integers, while keeping their sum.

#     Args:
#         n (List): List of floats.
#         min (int, optional): Minimum value of each element. Defaults to 1.

#     Returns:
#         List[int]: List of integers.
#     """
#     assert int(sum(n)) % 1 == 0, "Sum of list must be an integer."
#     n = np.asarray(n, dtype=float)  # type: ignore
#     n_r = np.around(n)
#     while sum(n_r) != sum(n):
#         inc = 1 if sum(n_r) < sum(n) else -1
#         idx = np.argmax(np.abs(n_r - n))
#         n_r[idx] += inc

#     return list(np.asarray(n_r, dtype=int))


def mean_absolute_error(x_orig: pd.DataFrame, x_pred: pd.DataFrame) -> np.ndarray:
    """Mean absolute error.

    Args:
        x_orig (pd.DataFrame): True labels.
        x_pred (pd.DataFrame): Predicted labels.

    Returns:
        ndarray[float]: Mean absolute error.
    """
    return np.mean(np.abs(x_orig - x_pred), axis=1)


def mean_squared_error(x_orig: pd.DataFrame, x_pred: pd.DataFrame) -> np.ndarray:
    """Mean squared error.

    Args:
        x_orig (pd.DataFrame): True labels.
        x_pred (pd.DataFrame): Predicted labels.

    Returns:
        ndarray[float]: Mean squared error.
    """
    return np.mean((x_orig - x_pred) ** 2, axis=1)


def root_mean_squared_error(x_orig: pd.DataFrame, x_pred: pd.DataFrame) -> np.ndarray:
    """Root mean squared error.

    Args:
        x_orig (pd.DataFrame): True labels.
        x_pred (pd.DataFrame): Predicted labels.

    Returns:
        ndarray[float]: Root mean squared error.
    """
    return np.sqrt(np.mean((x_orig - x_pred) ** 2, axis=1))


