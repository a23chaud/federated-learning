import numpy as np
import keras
from typing import List, Tuple, Dict
from numpy.typing import ArrayLike, NDArray
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
def eval_model(model: keras.Model, X, y) -> Tuple[NDArray, Dict[str, float or int]]:
    """Evaluate the model on the given data and return the confusion matrix and metrics"""

    loss, _ = model.evaluate(X, y, verbose=0)

    inferences = model.predict(X)
    y_pred = np.round(inferences).astype(int)
    y_true = y.to_numpy()

    cm = confusion_matrix(y_true, y_pred)

    if cm.shape != (2, 2):
        raise ValueError("Confusion matrix has an unexpected shape.")

    tn, fp, fn, tp = cm.ravel()

    return cm, {
        'accuracy': (tn + tp) / (tn + fp + fn + tp),
        'precision': tp / (tp + fp),
        'recall': tp / (tp + fn),
        'f1': 2 * tp / (2 * tp + fp + fn),
        'miss_rate': fn / (fn + tp)
    }, loss

def plot_cm(cm):
    plt.figure(figsize=(8, 8))
    plt.imshow(cm, cmap=plt.cm.Blues)
    for i in range(2):
        for j in range(2):
            plt.text(j, i, cm[i, j], horizontalalignment="center", color="white" if cm[i, j] > cm.max() / 2. else "black")
    plt.xlabel("Predicted label")
    plt.ylabel("True label")
    class_names = ['Normal', 'Attack']
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names)
    plt.yticks(tick_marks, class_names)
    plt.title("Confusion Matrix")
    plt.colorbar()
    plt.show()
