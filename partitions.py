import math
import numpy as np
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import random



#Partition 1 : One Dataset : Multiple Clients : Some Attack Classes
def partition_1(dataset_train,NUM_REMOVED_CLASSES,NUM_CLIENTS=10) :
    m_train = dataset_train[2]["Attack"]
    dataset_train.X, dataset_train.y, m_train = shuffle(dataset_train[0], dataset_train[1], m_train)

    # split the data into NUM_CLIENTS parts
    partition_size = math.floor(len(dataset_train.X) / NUM_CLIENTS)
    partitions = []

    for i in range(NUM_CLIENTS):
        idx_from, idx_to = i * partition_size, (i + 1) * partition_size

        X_part = dataset_train.X[idx_from:idx_to]
        y_part = dataset_train.y[idx_from:idx_to]
        m_part = m_train[idx_from:idx_to]

        # randomly remove NUM_REMOVED_CLASSES from each partition
        ATTACK_LABELS = m_train.unique()[m_train.unique() != 'Benign']
        _rm_classes = np.random.choice(ATTACK_LABELS, NUM_REMOVED_CLASSES, replace=False)

        X_part = X_part[m_part.isin(_rm_classes) == False]
        y_part = y_part[m_part.isin(_rm_classes) == False]
        m_part = m_part[m_part.isin(_rm_classes) == False]
        lacks = _rm_classes

        partitions.append((X_part, y_part, m_part, lacks))
    
    # plot the distribution of partitions
    _, axes = plt.subplots(1, NUM_CLIENTS, figsize=(15, 5))
    for i, ax in enumerate(axes):
        ax.set_title(f"Partition {i+1}")
        ax.set_ylabel("Count")
        ax.set_xlabel("Attack Label")
        ax.tick_params(axis='x', rotation=90)
        ax.bar(partitions[i][2].value_counts().index, partitions[i][2].value_counts())
    plt.show()
    
    return partitions

#Partition 2 : One Dataset : Multiple Clients : ALL Attack Classes
def partition_2(dataset_train, NUM_CLIENTS=10):
    m_train = dataset_train[2]["Attack"]
    dataset_train.X, dataset_train.y, m_train = shuffle(dataset_train.X, dataset_train.y, m_train)


    # Split the data into NUM_CLIENTS parts
    partition_size = math.floor(len(dataset_train.X) / NUM_CLIENTS)
    partitions = []

    for i in range(NUM_CLIENTS):
        idx_from, idx_to = i * partition_size, (i + 1) * partition_size

        X_part = dataset_train.X[idx_from:idx_to]
        y_part = dataset_train.y[idx_from:idx_to]
        m_part = m_train[idx_from:idx_to]

        partitions.append((X_part, y_part, m_part))
    
    # plot the distribution of partitions
    _, axes = plt.subplots(1, NUM_CLIENTS, figsize=(15, 5))
    for i, ax in enumerate(axes):
        ax.set_title(f"Partition {i+1}")
        ax.set_ylabel("Count")
        ax.set_xlabel("Attack Label")
        ax.tick_params(axis='x', rotation=90)
        ax.bar(partitions[i][2].value_counts().index, partitions[i][2].value_counts())
    plt.show()
    
    return partitions

#Partition 3: One Dataset : Multiple Clients : Each having exactly 1 attack label
def partition_3(dataset_train):
    m_train = dataset_train[2]["Attack"]
    unique_labels = m_train.unique()

    # Split the data into partitions
    partitions = []

    for label in unique_labels:
        label_indices = np.where(m_train == label)[0]
        X_part = dataset_train.X.iloc[label_indices]
        y_part = dataset_train.y.iloc[label_indices]
        m_part = m_train.iloc[label_indices]
        partitions.append((X_part, y_part, m_part))

    # plot the distribution of partitions
    _, axes = plt.subplots(1, len(unique_labels), figsize=(15, 5))
    for i, ax in enumerate(axes):
        ax.set_title(f"Partition {i+1}")
        ax.set_ylabel("Count")
        ax.set_xlabel("Attack Label")
        ax.tick_params(axis='x', rotation=90)
        ax.bar(partitions[i][2].value_counts().index, partitions[i][2].value_counts())

    plt.show()

    return partitions

#Partition 4 : One Client per dataset : All labels

def partition_4(datasets):
    partitions = []
    
    for i, dataset in enumerate(datasets):
        X = dataset[0]
        y = dataset[1]
        m = dataset[2]["Attack"]
        
        partition = (X, y, m)
        partitions.append(partition)
        
        # Plot the partition
        label_counts = m.value_counts()
        plt.figure(figsize=(6, 4))
        plt.bar(label_counts.index, label_counts.values)
        plt.title(f"Partition {i+1}")
        plt.xlabel("Attack Label")
        plt.ylabel("Count")
        plt.xticks(rotation=90)
        plt.show()
    
    return partitions


#Partition 5 : One client per dataset : Some labels : `NUM_REMOVED` labels randomly removed from each dataset
#By default NUM_REMOVED = 2 , but be careful it should not exceed the total number of classes of any dataset!
def partition_5(datasets,NUM_REMOVED=2):
    partitions = []
    
    for i, dataset in enumerate(datasets):
        X = dataset[0]
        y = dataset[1]
        m = dataset[2]["Attack"]
        
        # Randomly select two classes to remove
        classes_to_remove = random.sample(m.unique().tolist(), NUM_REMOVED)
        
        # Filter the dataset to remove the selected classes
        mask = ~m.isin(classes_to_remove)
        X_filtered = X[mask]
        y_filtered = y[mask]
        m_filtered = m[mask]
        
        partition = (X_filtered, y_filtered, m_filtered)
        partitions.append(partition)
        
        # Plot the partition
        label_counts = m_filtered.value_counts()
        plt.figure(figsize=(6, 4))
        plt.bar(label_counts.index, label_counts.values)
        plt.title(f"Partition {i+1}")
        plt.xlabel("Attack Label")
        plt.ylabel("Count")
        plt.xticks(rotation=90)
        plt.show()
    
    return partitions

#Partition 6 : Multiple client per dataset : 1 label per client within each dataset
def partition_6(datasets):
    partitions = []
    unique_labels = set()
    
    # Collect unique labels from all datasets
    for dataset in datasets:
        m = dataset[2]["Attack"]
        unique_labels.update(m.unique())
    
    # Create partitions with unique labels
    partition_number = 1
    for label in unique_labels:
        for i, dataset in enumerate(datasets):
            X = dataset[0]
            y = dataset[1]
            m = dataset[2]["Attack"]
            
            mask = m == label
            if mask.sum() > 0:
                X_filtered = X[mask]
                y_filtered = y[mask]
                m_filtered = m[mask]
                
                partition = (X_filtered, y_filtered, m_filtered)
                partitions.append(partition)
                
                # Plot the partition
                label_counts = m_filtered.value_counts()
                plt.figure(figsize=(6, 4))
                plt.bar(label_counts.index, label_counts.values)
                plt.title(f"Partition {partition_number}: {label}")
                plt.xlabel("Attack Label")
                plt.ylabel("Count")
                plt.xticks(rotation=90)
                plt.show()
                
                partition_number += 1
    
    return partitions

#Partition 7 : Multiple client per dataset : Each client has all the labels of the dataset
def partition_7(datasets, NUM_CLIENTS=10):
    partitions = []
    
    for dataset_idx, dataset in enumerate(datasets):
        X = dataset[0]
        y = dataset[1]
        m = dataset[2]["Attack"]
        
        # Split the dataset into NUM_CLIENTS parts
        partition_size = math.floor(len(X) / NUM_CLIENTS)
        dataset_partitions = []
        
        for i in range(NUM_CLIENTS):
            idx_from, idx_to = i * partition_size, (i + 1) * partition_size

            X_part = X[idx_from:idx_to]
            y_part = y[idx_from:idx_to]
            m_part = m[idx_from:idx_to]

            dataset_partitions.append((X_part, y_part, m_part))
        
        partitions.extend(dataset_partitions)
        
        # Plot the distribution of dataset partitions
        fig, axes = plt.subplots(1, NUM_CLIENTS, figsize=(15, 5))
        for i, ax in enumerate(axes):
            ax.set_title(f"Dataset {dataset_idx+1}, Partition {i+1}")
            ax.set_ylabel("Count")
            ax.set_xlabel("Attack Label")
            ax.tick_params(axis='x', rotation=90)
            ax.bar(dataset_partitions[i][2].value_counts().index, dataset_partitions[i][2].value_counts())
        plt.show()
    
    return partitions

#Partition 8 : Multiple client per dataset : Each client has {TOTAL-NUM_REMOVED_CLASSES} number of labels of that dataset
def partition_8(dataset_train, NUM_REMOVED_CLASSES, NUM_CLIENTS=10):
    partitions = []
    
    for dataset_idx, dataset in enumerate(dataset_train):
        X = dataset[0]
        y = dataset[1]
        m = dataset[2]["Attack"]
        
        # Shuffle the dataset
        X, y, m = shuffle(X, y, m)
        
        # Split the data into NUM_CLIENTS parts
        partition_size = math.floor(len(X) / NUM_CLIENTS)
        
        dataset_partitions = []
        
        for i in range(NUM_CLIENTS):
            idx_from, idx_to = i * partition_size, (i + 1) * partition_size

            X_part = X[idx_from:idx_to]
            y_part = y[idx_from:idx_to]
            m_part = m[idx_from:idx_to]
            
            # Randomly remove NUM_REMOVED_CLASSES from each partition
            unique_labels = m_part.unique()
            classes_to_remove = np.random.choice(unique_labels, NUM_REMOVED_CLASSES, replace=False)

            mask = ~m_part.isin(classes_to_remove)
            X_part = X_part[mask]
            y_part = y_part[mask]
            m_part = m_part[mask]
            
            dataset_partitions.append((X_part, y_part, m_part))
        
        partitions.extend(dataset_partitions)
        
        # Plot the distribution of dataset partitions
        fig, axes = plt.subplots(1, NUM_CLIENTS, figsize=(15, 5))
        for i, ax in enumerate(axes):
            ax.set_title(f"Dataset {dataset_idx+1}, Partition {i+1}")
            ax.set_ylabel("Count")
            ax.set_xlabel("Attack Label")
            ax.tick_params(axis='x', rotation=90)
            ax.bar(dataset_partitions[i][2].value_counts().index, dataset_partitions[i][2].value_counts())
        plt.show()
    
    return partitions
























