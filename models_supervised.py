import tensorflow
import keras

#ANN
def build_ANN(train_X) :

    model = keras.Sequential([
        keras.layers.Dense(128, activation='ReLU', input_shape=[len(train_X.columns)]),
        keras.layers.Dense(128, activation='ReLU'),
        keras.layers.Dense(1, activation='sigmoid')
    ])

    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=0.0001),
        loss='binary_crossentropy',
        metrics=['accuracy']
    )

    return model
#Decision Tree
from sklearn import tree
def build_decision_tree():

    model = tree.DecisionTreeClassifier(criterion="log_loss")

    return model

#Random Forest
from sklearn.ensemble import RandomForestClassifier
def build_random_forest():

    model = RandomForestClassifier(criterion="log_loss")

    return model

#K-NN
from sklearn.neighbors import KNeighborsClassifier
def build_knn():

    model = KNeighborsClassifier(n_neighbors=5)

    return model

#SVM
from sklearn.svm import SVC
def build_svm():

    model = SVC()

    return model

#Naive Bayes
from sklearn.naive_bayes import GaussianNB
def build_NB():

    model = GaussianNB()

    return model
